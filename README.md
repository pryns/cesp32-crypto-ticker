# cesp32 crypto ticker

Projecto de Crypto Ticker para la placa "TTGO T5 bluetooth Base ESP-32 ESP32 2.13 Pantalla de tinta electrónica"

# Uso
La placa se conecta a internet mediante wifi.

Se hacen 2 peticiones: una contra la API de coingecko para recibir los valores de las monedas introducidas y otra contra un reloj de uso horario en Madrid para pintar la hora.

## Getting started

1 - Descargar e instalar Arduino IDE (https://www.arduino.cc/en/software)

2 - Add libraries: 
- Ir a "File" > "Preferences" y en "Additional boards manager URLs" añadir: "https://dl.espressif.com/dl/package_esp32_index.json"
- Ahora desde "Tools" > "Board: ESP32 Dev module" > "Boards manager" instalar la "esp32" (by Espressif Systems)


### Connect the board
- No se necesitan drivers, windows debería ser capaz de reconocer el puerto serie de la placa para su uso, en caso contrario: https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers


De esta forma cuando seleccionemos Board y Port ya tendremos la "ESP32 Dev module" y el puerto COM correspondiente, algo como "COM3"

### Start coding
Para pintar nuestra salida haremos uso de la siguiente librería: https://github.com/lewisxhe/GxEPD

Descargamos el zip y vamos a "Sketch" > "Include library" > "Add ZIP library"

"Sketch" > "Include library"  > library manager e instalamos: "adafruit_gfx" (Adafruit GFX library by Adafruit)


### Serial port:
Tools > Serial monitor