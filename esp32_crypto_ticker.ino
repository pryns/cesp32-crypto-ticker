#include <GxEPD.h>
#include <GxDEPG0213BN/GxDEPG0213BN.h>    // 2.13" b/w 128x250, SSD1680, TTGO T5 V2.4.1, V2.3.1
// FONTS: https://learn.adafruit.com/adafruit-gfx-graphics-library/using-fonts
// #include <Fonts/FreeMonoBold18pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeSansBold9pt7b.h>
#include <Fonts/FreeSerifBold9pt7b.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

GxIO_Class io(SPI, /*CS=5*/ SS, /*DC=*/ 17, /*RST=*/ 16); // arbitrary selection of 17, 16
GxEPD_Class display(io, /*RST=*/ 16, /*BUSY=*/ 4); // arbitrary selection of (16), 4

const char *ssid = "Leia";
const char *password = "Leia@2021";
bool wifiConnected = false;

void setup() {
  Serial.begin(115200); // Iniciamos el puerto serial para debug
  Serial.println("Iniciando...");

  display.init();
  display.fillScreen(GxEPD_WHITE);
  display.setRotation(1);
  display.setTextColor(GxEPD_BLACK);
  display.setFont(&FreeMonoBold9pt7b);
  
  // Intenta conectar a WiFi
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Conectando a WiFi...");
  }
  
  wifiConnected = true;
  Serial.println("Conexión WiFi establecida");
  drawData();
}

void loop() {
  if (wifiConnected) {
    drawData();
    display.update();
    delay(30000); // Actualizar cada 30 segundos
  }
}

void getData() {
  HTTPClient http;
  http.begin("https://api.coingecko.com/api/v3/simple/price?ids=bitcoin,ethereum,dogecoin,binancecoin&vs_currencies=usd&include_24hr_change=true");

  String cryptocurrencies[] = {"bitcoin", "ethereum", "dogecoin", "binancecoin"};
  String cryptocurrenciesaliasses[] = {"BTC", "ETH", "DOGE", "BNB"};
  double prices[] = {0.0, 0.0, 0.0, 0.0};
  double changes[] = {0.0, 0.0, 0.0, 0.0};

  int httpCode = http.GET();
  if (httpCode == HTTP_CODE_OK) {
    String jsonStr = http.getString(); // Guardar en una variable para evitar error NPI
    StaticJsonDocument<2048> doc; // Ajusta el tamaño del documento según tus necesidades
    DeserializationError error = deserializeJson(doc, jsonStr);

    if (!error) {
      for (int i = 0; i < 4; i++) {
        prices[i] = doc[cryptocurrencies[i]]["usd"].as<double>();
        changes[i] = doc[cryptocurrencies[i]]["usd_24h_change"].as<double>();

        display.setFont(&FreeMonoBold12pt7b);
        display.setCursor(0, 45 + i * 20);
        display.print(cryptocurrenciesaliasses[i]); // NAME
        display.setCursor(80, 45 + i * 20);

        // Determine el número de decimales en función del valor de prices[i]
        int decimalPlaces = (prices[i] < 1.0) ? 3 : 0;
        char buf[10];
        dtostrf(prices[i], 2, decimalPlaces, buf);
        display.print(buf); // PRICE

        display.setCursor(165, 45 + i * 20);
        display.setFont(&FreeMonoBold9pt7b);
        display.print("(");
        if (changes[i] >= 0) {
          display.print("+");
        }
        display.print(String(changes[i], 1)); // 24H DIFF
        display.print(")");
      }
    } 
  }

  http.end(); // Cierra la conexión
}

String getTime() {
  HTTPClient http;
  http.begin("http://worldtimeapi.org/api/timezone/Europe/Madrid");

  int httpCode = http.GET();
  if (httpCode == HTTP_CODE_OK) {
    String jsonStr = http.getString(); // Guardar en variable que sino da error NPI
    StaticJsonDocument<1024> doc;
    DeserializationError error = deserializeJson(doc, jsonStr);

    if (!error) {
      const char* datetime = doc["datetime"];
      int year, month, day, hour, minute;
      sscanf(datetime, "%d-%d-%dT%d:%d", &year, &month, &day, &hour, &minute);

      http.end(); // Cierra la conexión

      // Formatea el minuto con un 0 delante si es un solo dígito
      String minuteStr = (minute < 10) ? "0" + String(minute) : String(minute);

      return String(hour) + ":" + minuteStr;
    } else {
      // Serial.println("Error: " + String(error.c_str()));
      return "err";
    }
  }
}

void drawData() {
  display.fillScreen(GxEPD_WHITE);

  display.setFont(&FreeMonoBold12pt7b);
  getData();

  display.setCursor(190, 17);
  display.setFont(&FreeMonoBold9pt7b);
  display.print(getTime());
}
